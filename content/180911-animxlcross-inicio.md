Title: Inicio de Animxlcross
Date: 2018-09-11
Cover: images/animxlcross.jpg

Animxlcross es un grupo de amigos interesados en viajar en bici y documentarlos. El logotipo hace referencia a las cabritas que tienen una gran capacidad de subir terrenos muy empinados, y la referencia es clara para cualquier ciclista que realiza viajes, tiene que realizar subidas en especial a las montañas.

![que es]({static}/images/animxlcross.jpg){:height="30%" width="30%"}

Los viajes en ciclocross siempre son relajantes y sobretodo te dan una libretad plena de entrar en contacto con la naturaleza y las personas amables en el camino. Nuestros viajes han sido a las serranias de nuestro Perú y la parte occidente de Bolivia.

![que es]({static}/images/anibal-puna.jpeg){:height="70%" width="70%"}


![que es]({static}/images/animxlcross-bol-desayuno.jpeg){:height="70%" width="70%"}

![que es]({static}/images/animxlcross-trenes.jpeg){:height="70%" width="70%"}

![que es]({static}/images/animxlcross-bol-basket.jpeg){:height="70%" width="70%"}
 
![que es]({static}/images/animxlcross-bol-perro.jpeg){:height="70%" width="70%"}

![que es]({static}/images/animxlcross-trenes.jpeg){:height="70%" width="70%"}
