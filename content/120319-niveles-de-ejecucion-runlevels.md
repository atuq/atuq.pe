Title: Niveles de ejecución (Runlevels)
Date: 2012-03-19
Cover: images/archlinux-grub-reboot.jpg

¿Para que nos sirve los niveles de ejecución?
Se puede aprovechar para entrar al sistema en modo shell tanto como modo usuario normal o modo de mantenimiento, la diferencia radica en la ejecución de los demonios, en el modo de mantenimiento se inicia con los demonios mínimos para correr shell y para el modo usuario normal si inicia con los demonios establecidos por el usuario.
Por tanto es de mucha ayuda por si tienes problemas en las iniciaciones de los demonios. Enumeración de los niveles de ejecución.

```text
1: Usuario único (modo mantenimiento)  Puedes usar si tienes problemas durante la iniciacion de tus demonios, etc.
2: No se utiliza.
3: Múltiples usuarios (modo normal)
4: No se utiliza.
5: Múltiples usuarios con X11: Igual que el nivel 3 pero con X11
6: Reinicio
0: Apagar
```

Pueden tomar nota en /etc/inittab para ver como trabajan.

La ejecución de Arch por defecto corre en nivel 3 (modo normal) bajo shell, pero la mayoría corre el sistema en el nivel donde sea más sencillo de manejar las aplicaciones y archivos de manera gráfica por tanto se está haciendo uso de un entorno de escritorio o un gestor de ventana en solitario, ambos corriendo con X11, ése es el nivel 5.


Estos niveles de ejecución pueden ser modificados durante la iniciación en el menú de grub, el sistema espera 5s o el tiempo que hayas considerado para iniciarla. Se cancela la secuencia de tiempo mediante la presión de las teclas flechas.
Tenemos varias opciones:

1. Presionar enter para iniciar el sistema seleccionado.
2. Presionar "e" para editar las líneas de comandos.
3. Presionar "c" para agregar una línea de comando

![que es]({static}/images/archlinux-grub-reboot.jpg)

Si queremos iniciar los niveles de ejecución, tenemos que modificar la segunda línea de comando, para eso seleccionamos la segunda línea y presionamos "e" para editar, luego agregamos delante de kernel /vmlinuz-linux root =/dev/sda3 ro el número del nivel que queremos iniciar, presionamos enter para guardar e iniciar el boot presionando "b"

Ejemplo: (Inicio del sistema en nivel 1)

```text
kernel /vmlinuz-linux root=/dev/sda3 ro 1
```
![que es]({static}/images/icons/avatar.png)

Nota 1: Los cambios de nivel de ejecución durante la iniciación en el menú grub no son guardados en el archivo del sistema, por tanto solo sirven como un método de entrada temporal hasta que se termine el sistema.
Para cambiar el nivel de ejecución se modifica el archivo: /etc/inittab
 
