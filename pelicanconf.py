#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

#AUTHOR = 'Atuq'
SITENAME = "Atuq"
DOMAIN = "atuq.pe"
BIO_TEXT = 'Ingeniería electrónica y bikepacker de Ayacucho, PE.'
FOOTER_TEXT = 'Desarrollado con <a href="http://getpelican.com">Pelican</a>.'

SITE_AUTHOR = 'Atuq'
INDEX_DESCRIPTION = 'Website y blog de Atuq, Ingeniero Electrónico y ciclista aventurero de Ayacucho, PE.'
#SITEURL = 'http://atuq.pe'

SIDEBAR_LINKS = [
    '<a href="/yo/">Sobre mí</a>',
    '<a href="/archivo/">Archivo</a>',
]

ICONS_PATH = 'images/icons'

SOCIAL_ICONS = [
    ('mailto:me@atuq.pe', 'Email (me@atuq.pe)', 'fa-envelope'),
    ('http://twitter.com/puercoarana', 'Twitter', 'fa-twitter'),
#    ('http://github.com/iKevinY', 'GitHub', 'fa-github'),
#    ('http://soundcloud.com/iKevinY', 'SoundCloud', 'fa-soundcloud'),
    ('http://instagram.com/animxlcross', 'Instagram', 'fa-instagram'),
]

THEME_COLOR = '#FF8000'


RELATIVE_URLS = True
SITEURL = 'https://atuq.pe'
TIMEZONE = 'America/Lima'
DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%B %d, %Y'
#DEFAULT_PAGINATION = 10
DEFAULT_PAGINATION = False
DEFAULT_LANG = 'en'
SUMMARY_MAX_LENGTH = 82

THEME = '/home/davidvg3d/virtualenvs/atuqpe/www/pelican-themes/pneumatic'

ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'

ARCHIVES_SAVE_AS = 'archivo/index.html'
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

DIRECT_TEMPLATES = ['index', 'archives']
CATEGORY_SAVE_AS = ''

FEED_ATOM = 'atom.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

TYPOGRIFY = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.admonition': {},
        'markdown.extensions.codehilite': {'linenums': None},
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5',
}

CACHE_CONTENT = False
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = 'output'
PATH = 'content'

#ICONS_PATHS = ''
#MD_EXTENSIONS = ['codehilite(linenums=None)']

templates = ['404.html']
TEMPLATE_PAGES = {page: page for page in templates}

STATIC_PATHS = ['images', 'files', 'extra']
IGNORE_FILES = ['.DS_Store', 'pneumatic.scss', 'pygments.css']

extras = ['favicon.ico', 'robots.txt']
EXTRA_PATH_METADATA = {'extra/%s' % file: {'path': file} for file in extras}

PLUGIN_PATHS = ['/home/davidvg3d/virtualenvs/atuqpe/www/pelican-plugins']
PLUGINS = ['assets','advthumbnailer', 'neighbors', 'render_math']
ASSET_SOURCE_PATHS = ['static']
ASSET_CONFIG = [
    ('cache', False),
    ('manifest', False),
    ('url_expire', False),
    ('versions', False),
# Feed generation is usually not desired when developing
#AUTHOR_FEED_ATOM = None
#AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

#SOCIAL = (('twitter', 'https://twitter.com/yourprofile'),
#         ('instagram', 'https://instagram.com/yourprofile'),
#         ('github', 'https://github.com/yourprofile'))

# Uncomment following line if you want document-relative URLs when developing



]
