Title: Mi niñez en Huanca Sancos
Date: 2017-07-21
Cover: images/abuelo-yo-vaso.jpeg

Sancos es un pueblo ubicado en la provincia de Huanca Sancos, el pueblo que me vió nacer, crecer y compartir las costumbres ancestrales de mis padres, abuelos y nuestros ancestros chankas.

Los mejores recuerdos sin duda están en mi pueblo, quizá por la nostalgia que todos podemos sentir por nuestra infancia libre de preocupaciones, angustias y necesidades que podemos sentir en nuestra época y conscientes a nuestra edad, sin embargo siempre he sentido la necesidad de regresar y ayudar a la comunidad. En Lima es diferente la sociedad, suelen ser mas independientes, llevan una vida acelerada, la competencia que se vive entre todos por obtener un cupo de estudio, trabajo e incluso hasta ser reconocido como alguien "exitoso".  

![que es]({static}/images/abuelo-yo-vaso.jpeg){:height="70%" width="70%"}
En esta foto junto a mi abuejo tendría unos 6 años.
La generación de mis abuelos hicieron mucho por el pueblo, mis abuelos maternos eran profesores además de campesinos, enseñaban en el mismo Sancos o a veces les tocaba enseñar en anexos un poco lejanos como en Colcabamba donde mi abuela enseñó un buen tiempo, lugar donde mi tía y mi mamá crecieron, y finalmente mis abuelos paternos también eran campesinos al igual que todo el pueblo, se dedicaron a la tierra y el ganado. 


![que es]({static}/images/campo-hcos3.jpeg){:height="70%" width="70%"}

Con esto no implico que en mi pueblo sea económicamente estable, al contrario, mi pueblo es uno de los mas pobres de Ayacucho, agregando que hace un año recién se ha construido una pista que una a Huamanga, eso dibuja el triste olvido que tiene la región sur del Perú de parte del grupo político de Lima. A lo que me refiero es que encuentro el calor familiar, el sentido de comunidad, las fiestas, las faenas comunales que en runasimi sería a minka o las faenas familiares que serían el ayni, dos sistemas de trabajo comunitario que hasta ahora se siguen practicando además de conserver aún la organización por Ayllus desde tiempos inmemorables, cuatro ayllys que agrupan a las familias de Sancos y que van pasando de generación a generación. 

![que es]({static}/images/campo-hcos4.jpeg){:height="70%" width="70%"}
Esta foto es el ayni, ahí estan cuatro familias que unen lazos de comadres, compadres o familiares. Tendría 7 años, estoy ubicado entre mi abuela Mamá América y mi abuelo Papa Adalberto, cargando a mi pequeña prima. Hacia el extremo izquierdo está mi abuela paterna Mamá Segundina, junto a mi hermana Mónica y a su vez al lado de Mama América.

Aún recuerdo que crecí sin luz, usabamos bastante la vela o mechero. Mi abuelo hacía sus velas, recuerdo que tenía su cuarto donde los hacía, al lado del cuarto donde estaba la cocina. Nos prohibía la entrada, mis primas y yo lo veíamos como un cuarto místico por tal prohibición, además de ver de lejos como colgaban las velas de unos tenderos.

![que es]({static}/images/fiesta-hcos-abuelo.jpeg){:height="70%" width="70%"}
Pese a las carencias, el trabajo duro al campo y las faenas en beneficio de la comunidad, siempre había tiempo para las fiestas del pueblo como en esta foto donde se ve a mi abuelo en una gran ronda en la plaza del pueblo y la nostálgica y alegre música que siempre lo llevo a todos lados donde vaya y que me entra piedritas al ojo recordando mi pueblo y mi familia cada vez que lo escucho lejos de ellos.
 
Es necesario que los profesionales que tengan un poco de interés en cambiar las cosas como están regidas en este sistema, puedan hacer el cambio justamente donde se requiere que haya cambios, y es la educación que nos hará crecer, más que nada en los pueblos más alejados, yo he tenido mucha suerte en llegar hasta la Pontificia Universidad Católica del Perú, sin embargo esto no hará que claudique mi idea de regresar y enseñar pese a la oposición de mis padres que tristemente como en toda familia inmigrante, induce en quedarse y no regresar a un pueblo "triste", por obvias razones que los abuelos ya no están, las ventajas del clima, el trabajo duro al campo, y cosas similares. Yo no lo veo así, uno se educa en una universidad para contribuir en un cambio a la comunidad, no para trabajar para intereses de capitales extranjeros, lo veo sin ningún propósito relevante excepto que el de ganar dinero para un fin personal.

![que es]({static}/images/escuela-desayuno-hcos.jpeg){:height="70%" width="70%"}
Foto tomada junto a mis compañeros en pleno desayuno comunitario que se daba en la escuela N°38490 de Huanca Sancos.
