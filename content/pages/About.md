Title: Sobre mí
Slug: yo

Estudiante de Ingeniería Electrónica en la Pontificia Universidad Católica del Perú.
Soy un entusiasta desarrollador y un bikepacker de [Ayacucho, Perú](https://en.wikipedia.org/wiki/Ayacucho).

Este sitio web es el lugar donde encontrarás todas las cosas que pasan por mi cabeza.

Inventor asociado de Dinamómetro Geriátrico, proyecto distinguido en Indecopi y la PUCP. Actualmente vengo desarrollando un proyecto independiente ligado a la medicina.

Este blog es generado con [Pelican](http://getpelican.com) usando [Pneumatic](https://github.com/iKevinY/pneumatic).
